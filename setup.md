# Getting Started
 It is very easy to integrate BeyondChats Widget on your website. Just follow the steps below and you are good to go.

## Installation

* ### Step 1: Get your Chat Bot url
> &nbsp;&nbsp; Go to [Admin panel](https://beyond-chats.netlify.app/admin) and copy the **Source Code** from the **Setup BeyondChats** section. <br>
<div
  style="
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    max-width: 768px;
    max-height: 480px;
    border-radius: 10px;
  "
>
  <img src="/assets/admin.png "  style="width:100%; height:100%; max-width:768px; max-height:480px; border-radius: 10px;"
  />
</div>

## Step 2: Add the script to your website
&nbsp;&nbsp; Add the copied script tag to the end of **body** tag of your main **html** file.

```html
<script src="https://chatintegration.beyondexams.org/chat-widget?orgId=<!--YOUR-ORG-ID-->"></script>
```
<div
  style="
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    max-width: 840px;
    max-height: 480px;
    border-radius: 10px;
  "
>
  <img src="/assets/embed.png "  style="width:100%; height:100%; max-width:840px; max-height:480px; border-radius: 10px;"
  />
</div>

* ### Step 3: You are done! :heart_eyes:
&nbsp;&nbsp; You have successfully integrated BeyondChats Widget on your website. You should see the chat head on the bottom right corner of your website. <br>
> Result: <br>
<div
  style="
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    max-width: 360px;
    gap: 10px;
    max-height: 500px;
    border-radius: 10px;
  "
>
  <img src="/assets/result.gif"  style="width:100%; height:100%; max-width:360px; max-height:500px; border-radius: 10px;" />
</div>

<hr>


## Customization  
> Go to [Admin panel](https://beyond-chats.netlify.app/admin) to customize the Chat Bot :fire:


## For AMP Websites
* ### Step 1: Add the script tag to add amp-iframe
&nbsp;&nbsp; Add the following script tag to the end of **body** tag of your main **html** file.
```html
<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
```
* ### Step 2: Add the iframe tag
Add the following code at below the script tag
```
<!-- Start BE Chat Embed iFrame -->
    <amp-iframe
      src="https://chatintegration.beyondexams.org/amp?name=IVF Chat Support&themeBackground=lightblue&themeColor=black&chatColor=black&chatBackground=cyan&img=&fontDifference=0&positionBottom=10&positionRight=10&showBubble=true&askEmail=true"
      id="be"
      style="border: 0"
      class="be_chat_embed_container"
      allowfullscreen
      resizable
      frameborder="0"
      width="80"
      height="80"
      scrolling="no"
      sandbox="allow-scripts allow-same-origin allow-forms "
      [src]="beChatEmbedUrl"
    >
      <div overflow tabindex="0" role="button" aria-label="Show more">
        Chat
      </div>
    </amp-iframe>
  <!-- End BE Chat Embed iFrame -->
```

### To customize the chat widget
Add the following to style tag of your AMP web page
```css
  /* Start BE Chat Embed CSS*/
    .be_chat_embed_container {
      position: fixed;
      right: 25px; /* Change Chat Embed Position Here*/
      bottom: 60px; /* Change Chat Embed Position Here*/
      overflow: hidden;
      z-index: 1300;
    }
  /* End BE Chat Embed CSS */
```

