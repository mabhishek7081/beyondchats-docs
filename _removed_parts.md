## Installation with Customizations

* ### Step 1: Add the script tag
&nbsp;&nbsp; Add the following script tag to the end of **body** tag of your main **html** file.
```html
<!-- Start BE Chat Embed Script -->
    <script defer>
			window.addEventListener("load",() => {
				const src = new URL(
					"https://chatintegration.beyondexams.org/chat-widget"
				);
        const script = document.createElement( 'script' );
        script.setAttribute( 'src', src.toString() );
        document.body.appendChild( script );
			}, {once: true});
		</script>
<!-- End BE Chat Embed Script -->
```
* ### Step 2: Customize the chat widget as per your need
&nbsp;&nbsp; To add customizations to the chat widget, modify the script tag <br>
&nbsp;&nbsp; Add the following script text 
  ```js
  src.searchParams.set("name", "BE Support"); // Your Company Name Here
  ```
&nbsp;&nbsp; It will look something like this

  ```js
<!-- Start BE Chat Embed Script -->
    <script defer>
      window.addEventListener("load",() => {
        const src = new URL(
          "https://chatintegration.beyondexams.org/chat-widget"
        );
        src.searchParams.set("name", "BE Support"); // Your Company Name Here
        
        const script = document.createElement( 'script' );
        script.setAttribute( 'src', src.toString() );
        document.body.appendChild( script );
      }, {once: true});
    </script>
<!-- End BE Chat Embed Script -->
  ```
&nbsp;&nbsp; **Explanation:** The **name** parameter is used to set the name of your orgnization to Chat window <br>

### Result
&nbsp;&nbsp; You should see your orgnization name on the chat window.
<div
  style="
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    max-width: 480px;
    max-height: 840px;
    border-radius: 10px;
  "
>
  <img src="/assets/custom_name.png"  style="width:100%; height:100%; max-width:500px; max-height:840px; border-radius: 10px;"
  />
</div>


<!-- ### Customization Examples 
<div
  style="
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    max-width: 480px;
    max-height: 840px;
    border-radius: 10px;
  "
>
  <img src="/assets/options.png"  style="width:100%; height:100%; max-width:500px; max-height:840px; border-radius: 10px;"
  />
</div>


&nbsp;&nbsp; 1. Change the Name of the chat widget
```js
src.searchParams.set("name", "BE Support"); 
```
&nbsp;&nbsp; 2. Change the theme color of the chat widget
```js
src.searchParams.set("themeColor", "#fff");
```
&nbsp;&nbsp; 3. Change the Welcome Message of the chat bot 
```js
src.searchParams.set("welcomeText", ['Heyo! Welcome to xyz support']); 
```
&nbsp;&nbsp; 4. Change the logo of chat bot
```js
src.searchParams.set("img", "https://chatintegration.beyondexams.org/assets/logo.png");
``` -->