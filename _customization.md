<!-- # Customization -->

## Base Customization
### 1. Name :id=name
&nbsp;&nbsp; To change the name of the chat bot, use the following code snippet.
```js
src.searchParams.set("name", "BE Support"); 
```
> type: string | example: "BE Support"

### 2. Welcome Message :id=welcome-message
&nbsp;&nbsp; This will change the welcome message of the chat bot. which is the first message that the user will see when they open the chat bot.
<br>
<!-- &nbsp;&nbsp; **Note:** `This is an array of strings, so you can add multiple messages. -->
```js
src.searchParams.set("welcomeText", JSON.stringify(["Heyo! Welcome to xyz support", "How can I help you?"])); 
```
> type: array | example: JSON.stringify(["Heyo! Welcome to xyz support", "How can I help you?"])
<!-- | type | example |
| --- | --- |
<!-- | string| "Heyo! Welcome to xyz support" | 
|array with multiple messages | "[Heyo! Welcome to xyz support", "How can I help you?"]" | -->

### 3.  Custom Icon :id=icon
&nbsp;&nbsp; To change the icon of the chat bot, use the following code snippet.
```js
src.searchParams.set("img", "https://chatintegration.beyondexams.org/assets/logo.png");
```
> type: image URL | example: "https://chatintegration.beyondexams.org/assets/logo.png"

---
## Chat Bot
### 4. Theme Color :id=theme-color
&nbsp;&nbsp; To change the theme color of the chat bot, this will change color of
- Name  
- Buttons
- Chat Head icon
- messages sent by the chat bot
```js
src.searchParams.set("themeColor", "#fff");
```
| type | example |
| --- | --- |
| string | "red" |
| hex | "#ffffff" |
| rgb | "rgb(255, 255, 255)" |



### 5. Theme Background Color :id=theme-background-color
&nbsp;&nbsp; To change the background color of 
- The messages sent by Chat Bot
- Chat window header
- Chat Head 
```js
src.searchParams.set("themeBackground", "red");
```
| type | example |
| --- | --- |
| string | "red" |
| hex | "#ffffff" |
| rgb | "rgb(255, 255, 255)" |


## User Message

### 6. Chat Background Color :id=chat-background-color
&nbsp;&nbsp; To change the background color of meassages sent by the user.
```js
src.searchParams.set("chatBackground", "skyblue");
```
| type | example |
| --- | --- |
| string | "skyblue" |
| hex | "#ffffff" |
| rgb | "rgb(255, 255, 255)" |

### 7. Chat Color :id=chat-color
&nbsp;&nbsp; To change the color of the messages sent by the user.
```js
src.searchParams.set("chatColor", "white");
```
| type | example |
| --- | --- |
| string | "white" |
| hex | "#ffffff" |
| rgb | "rgb(255, 255, 255)" |

## Visual
### 8. Font Difference :id=font-difference
&nbsp;&nbsp; To change the font of the chat bot, you can increase or decrease the font size.
```js
src.searchParams.set("fontDifference", -1); // -1 for decrease and 1 for increase
```
| value | effect |
| --- | --- |
| -1 | decrease font size by 1 |
| 5 | increase font size by 5 |
|+1 | increase font size  by 1|

### 9. Position Left :id=position-left
&nbsp;&nbsp; To change the position of the chat bot from left.
```js
src.searchParams.set("positionLeft", auto);
```
| type | example |
| --- | --- |
| string | "auto" |
| number | 100 |

### 10. Position Right :id=position-right
&nbsp;&nbsp; To change the position of the chat bot from right.
```js
src.searchParams.set("positionRight", 36);
```
| type | example |
| --- | --- |
| string | "auto" |
| number | 100 |

### 11. Position Bottom :id=position-bottom
&nbsp;&nbsp; To change the position of the chat bot from bottom.
```js
src.searchParams.set("positionBottom", 25);
```
| type | example |
| --- | --- |
| string | "auto" |
| number | 100 |


## Advanced

### 12. Show Chat Head :id=show-chat-head
&nbsp;&nbsp; To show or hide the chat head.
<br>&nbsp;&nbsp; **Note:** This will not hide the chat bot, it will only hide the chat head. You can still open the chat bot by  [custom button](/customization.md?id=custom-button) 

```js
src.searchParams.set("showBubble", false);
```
> type: boolean | example: false, true

### 13. Ask Email :id=ask-email
&nbsp;&nbsp; To ask the user for email address or not. If No is selected, then the user will not be asked for email address and it will open the chat bot directly.
```js
src.searchParams.set("askEmail", false);
```
> type: boolean | example: false, true

### 14. Pre-populated Questions :id=pre-populate-questions
&nbsp;&nbsp; To provide some pre-populated questions to the user. This will show the user some questions and the user can select any one of them.
```js
src.searchParams.set("prePopulateQuestions", JSON.stringify(["How to login?", "How to register?"]));
```
> type: array | example: JSON.stringify(["How to login?", "How to register?"])

### 15. Loader :id=loader
&nbsp;&nbsp; To change the loader of the chat bot. It will shown every time the chat bot is loading.
> As of now it only supports GIF with transparent background and height of 16px.
```js
src.searchParams.set("loader", "https://chatintegration.beyondexams.org/assets/loader.gif");
```
> type: GIF URL | example: "https://chatintegration.beyondexams.org/assets/loader.gif"
<div
  style="
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    max-width: 360px;
    max-height: 600px;
    border-radius: 10px;
  "
>
  <img src="/assets/customloader.gif"  style="width:100%; height:100%; max-width:360px; max-height:600px; border-radius: 10px;"
  />
</div>

### 16. Custom Button :id=custom-button
&nbsp;&nbsp; To add a custom button to toggle the chat bot in your website.
```js
// You can toggle the chat bot from anywhere in your website by calling toggleBeChatEmbed() function.
// This function will toggle the chat bot.

<button onclick="toggleBeChatEmbed()"> Chat</button>
```
<div
  style="
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    max-width: 360px;
    max-height: 600px;
    border-radius: 10px;
    margin-bottom: 16px
  "
>
  <img src="/assets/customfunction.gif"  style="width:100%; height:100%; max-width:360px; max-height:600px; border-radius: 10px;"
  />
</div>


## Example
  > setup with custom config

  ```html
  <html>
	<head>
	</head>

	<body>
		<!-- Start BE Chat Embed Script -->
		  <script defer>
		    window.addEventListener("load",() => {
		      const src = new URL(
		        "https://chatintegration.beyondexams.org/chat-widget"
		      );
		      src.searchParams.set("name", "Tomato"); 
		      src.searchParams.set("welcomeText", JSON.stringify(["Heyo! Welcome to Tomato support", "How can I help you?"])); 
		      src.searchParams.set("img", "https://cdns.iconmonstr.com/wp-content/releases/preview/2013/240/iconmonstr-smiley-2.png")
		      src.searchParams.set("themeColor", "#fff");
		      src.searchParams.set("themeBackground", "tomato");
		      src.searchParams.set("chatBackground", "skyblue");
		      src.searchParams.set("chatColor", "white");
		      src.searchParams.set("fontDifference", 1); 
		      src.searchParams.set("positionLeft", "auto");
		      src.searchParams.set("positionRight", 36);
		      src.searchParams.set("positionBottom", 25);
		      src.searchParams.set("showBubble", true);
		      src.searchParams.set("askEmail", false);
		      src.searchParams.set("loader", "https://chatintegration.beyondexams.org/assets/loader.gif");
		      const script = document.createElement( 'script' );
		      script.setAttribute( 'src', src.toString() );
		      document.body.appendChild( script );
		    }, {once: true});
		  </script>
		  
		<!-- End BE Chat Embed Script -->
	</body>
</html>
```

<div
  style="
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    max-width: 360px;
    max-height: 600px;
    border-radius: 10px;
  "
>
  <img src="/assets/final.png"  style="width:100%; height:100%; max-width:480px; max-height:600px; border-radius: 10px;"
  />
</div>
